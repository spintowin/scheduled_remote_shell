class IExecutable:
    async def run(self):
        raise NotImplementedError()

class OperationRunner:
    def __init__(self, operations: [Operation]):
        super().__init__()
    def run(self) -> None:
        pass

class AutoCommandReaderOperation(Operation):
    def __init__(self, async_reader: AsyncReader, telemetries_writer: TelemetriesWriter, inovoker: IInvoker):
        super().__init__()
        self._reader = async_reader
    async def run(self):
        async for command in self._reader:
            self._telemetries_writer.add(command)
            self._invoker.invoke(command)

class Operation(IExecutable):
    pass

class IAction(IExecutable):
    def __init__(self, task: asyncio.Task):
        super().__init__()
    async def wait_for_event(self) -> None:
        pass
    async def run(self) -> None:
        pass

class ScheduledAction(Action):
    def __init__(self, date_to_run: datetime, task: asyncio.Task):
        super().__init__()
    async def wait_for_event(self) -> None:
        pass

class EventedAction(Action):
    def __init__(self, date_to_run: asyncio.Event, task: asyncio.Task):
        super().__init__()
    async def wait_for_event(self) -> None:
        pass

class AsyncReader:
    async def __aiter__(self):
        pass
    async def __anext__(self) -> ProtobufMessage:
        pass

class TelemetriesWriter:
    def __init__(self, writer: StreamWriter):
        super().__init__()
    def write_telemetry(self, telemetry: ProtobufMessage) -> None:
        pass

class IInvoker:
    def invoke(self, command: Command) -> IAction:
        pass

class CommandInvoker(IInvoker):
    def __init__(self, command_handlers: [ICommandHandler]):
        super().__init__()

class ICommandHandler:
    def get_command_type(self) -> int:
        pass
    async def handle(self, command: Command) -> None:
        pass


class ExecuteBashCommandHandler(ICommandHandler):
    async def handle(self, command: Command):   
        return super().handle(command)

def main():
    operation_runner = OperationRunner([AutoFileReaderOperation()])
    operation_runner.run()
