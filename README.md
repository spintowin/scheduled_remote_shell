After cloning the repository:   
1. Create a venv for the project and activate it.   
2. ```pip install -e .```   

When new requirements are added, please remember to add them to setup.py and verify that pip install does everything correctly, including the installation of the dependencies.
