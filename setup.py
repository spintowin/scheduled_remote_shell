import shutil
import setuptools
import os
import subprocess


def compile_protobufs():
    # These messages can only be seen if something goes wrong
    print('Compiling protobufs...')
    project_root = os.path.dirname(os.path.abspath(__file__))
    protos_path = os.path.join(project_root, 'scheduled_remote_shell', 'protos')
    tmp_protopy_path = os.path.join(project_root, 'protopy_temp')

    try:
        for proto_dir in ['commands']:
            subprocess.check_output(['protopy', '-d', os.path.join(protos_path, proto_dir), '-o', tmp_protopy_path], stderr=subprocess.STDOUT)
            subprocess.check_output(['tar', '-xf', os.path.join(tmp_protopy_path, '1.0.0-1.0.0.tar.gz'), '-C', tmp_protopy_path], stderr=subprocess.STDOUT)
            for root, dirs, files in os.walk(tmp_protopy_path):
                for f in files:
                    if '_pb2.py' in f:
                        dst_path = os.path.join(protos_path, proto_dir, f)
                        shutil.copyfile(os.path.join(root, f), dst_path)
                        subprocess.check_output(['2to3', '-w', dst_path])
    finally:
        shutil.rmtree(tmp_protopy_path, ignore_errors=True)
        shutil.rmtree(os.path.join(project_root, '1.0.0'), ignore_errors=True)
        shutil.rmtree(os.path.join(protos_path, '1.0.0'), ignore_errors=True)


with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="scheduled-remote-shell", # Replace with your own username
    version="0.0.1",
    author="Example Author",
    author_email="author@example.com",
    description="Scheduled Remote Shell.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    entry_points={
        'console_scripts': [
            'scheduled_remote_shell = scheduled_remote_shell.main:main'
        ]
    },
    install_requires=[
        'asyncssh==2.2.0',
        'aiorun==2020.2.1',
        '2to3',
        'protobuf3',
        'protobuf-compiler',
        'pytest-asyncio'
    ],
    python_requires='>=3.7',
)


compile_protobufs()