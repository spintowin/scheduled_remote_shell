import io
from abc import abstractmethod


class AsyncWriterStreamIsNotWritableException(BaseException):
    pass


class AsyncWriterBase:
    def __init__(self, stream: io.IOBase):
        self._stream = stream

    @abstractmethod
    async def write(self, data: str):
        if not self._stream.writable():
            raise AsyncWriterStreamIsNotWritableException(self._stream)

    async def close(self):
        self._stream.close()

