import io

from scheduled_remote_shell.framework.streams.writer import async_writer_base


class AsyncTextWriter(async_writer_base.AsyncWriterBase):
    def __init__(self, stream: io.TextIOBase):
        super().__init__(stream)

    async def write(self, data: str):
        await super().write(data)
        self._stream.write(data)

