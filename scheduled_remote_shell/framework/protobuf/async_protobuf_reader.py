from protobuf3.message import Message
from aiofile import AIOFile
from typing import Union
from asyncio import StreamReader


class AsyncProtobufReader:
    def __init__(self, protobuf_object: Message, protobuf_stream: Union[StreamReader, AIOFile]):
        self._protobuf_object = protobuf_object
        self._protobuf_stream = protobuf_stream

    async def read(self) -> Message:
        data = await self._protobuf_stream.read()

        return self._protobuf_object.create_from_bytes(data)
