import asyncio
import asyncssh


class SSHClient:
    def __init__(self, server_ip: str, server_port: int, username: str, password: str, timeout: int = None, *args, **kwargs):
        self._server_ip = server_ip
        self._server_port = server_port
        self._username = username
        self._password = password
        self._connection = None
        self._connection_timeout = timeout
        self._args = args
        self._kwargs = kwargs

    async def __aenter__(self):
        try:
            self._connection = await asyncio.wait_for(
                asyncssh.connect(self._server_ip, self._server_port, username=self._username,
                                password=self._password, *self._args, **self._kwargs),
                self._connection_timeout)
            return self
        except asyncio.TimeoutError:
            print("Connection creation with {self._server_ip}:{self._server_port} has timed out and cancelled.")
            raise

    async def __aexit__(self, *exc_params):
        print("closing the ssh connection")
        self._connection.close()
        await self._connection.wait_closed()

    async def execute(self, command: str, **kwargs) -> str:
        print(f"executing {command}")
        return await self._connection.run(command, **kwargs)


# async def main(username, password):
#     async with SSHClient('0.0.0.0', 2222, username, password) as connection:
#         await asyncio.gather(
#             connection.execute("dir"),
#             connection.execute("sleep 10 && echo \"Woke up from sleep\""),
#             connection.execute("whoami"),
#             connection.execute('ls /'))

# if __name__ == '__main__':
#     import sys
#     if len(sys.argv) == 3:
#         asyncio.run(main(sys.argv[1], sys.argv[2]))
#     else:
#         print("username and password required")
