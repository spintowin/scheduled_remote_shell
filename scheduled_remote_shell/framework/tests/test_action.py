import asyncio
import datetime

import pytest

from scheduled_remote_shell.framework.executables.action.iaction import IAction
from scheduled_remote_shell.framework.executables.action.immediate_action import ImmediateAction
from scheduled_remote_shell.framework.executables.action.scheduled_action import ScheduledAction


async def get_executed_datetime(action_executed: asyncio.Future) -> int:
    action_executed.set_result(True)
    return datetime.datetime.now()


@pytest.mark.asyncio
async def test_immediate_action_sanity():
    action_executed = asyncio.Future()
    before = datetime.datetime.now()
    action = ImmediateAction(get_executed_datetime(action_executed))
    action.start()
    after = await action
    delta = after - before
    assert action_executed.result()
    assert delta.seconds < 1


@pytest.mark.asyncio
async def test_scheduled_action_sanity():
    action_executed = asyncio.Future()
    before = datetime.datetime.now()
    sleeping_time = 5
    scheduled_time = datetime.datetime.now() + datetime.timedelta(seconds=sleeping_time)
    action = ScheduledAction(get_executed_datetime(action_executed), scheduled_time)
    action.start()
    after = await action
    delta = after - before
    assert action_executed.result()
    assert delta.seconds < sleeping_time + 1 # maring of 1 seconds
