from abc import abstractmethod, abstractstaticmethod
from typing import Type, TypeVar

T = TypeVar('T')
class ICommandHandler:
    @abstractstaticmethod
    def get_command_type() -> Type[T]:
        pass

    @abstractmethod
    async def handle(self, command: T) -> None:
        pass
