from typing import Type

from scheduled_remote_shell.framework.command_handler.icommand_handler import ICommandHandler
from scheduled_remote_shell.protos.commands.execute_bash_command_pb2 import ExecuteBashCommand
from scheduled_remote_shell.framework.connections.ssh import SSHClient
from scheduled_remote_shell.framework.telemetries.telemetries_writer import TelemetriesWriter


class ExecuteBashCommandHandler(ICommandHandler):
    def get_command_type(self) -> Type[ExecuteBashCommand]:
        return ExecuteBashCommand

    async def handle(self, command: ExecuteBashCommand) -> None:
        async with SSHClient(command.server_ip, command.server_port,
                             command.username, command.password) as ssh_client:
            for bash_command in command.commands:
                result = await ssh_client.execute(bash_command)
                TelemetriesWriter.write(f'Executed: {bash_command}\n Result: {result}\n')

