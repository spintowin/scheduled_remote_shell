from scheduled_remote_shell.framework.executables.action.iaction import IAction


class ImmediateAction(IAction):
    async def wait_to_run(self):
        return
