import asyncio
from abc import abstractmethod
from typing import Coroutine

from scheduled_remote_shell.framework.executables.base.iexecutable import IExecutable


class IAction(IExecutable):
    def __init__(self, action: Coroutine):
        super().__init__()
        self._action = action
        self._action_task = None

    @abstractmethod
    async def wait_to_run(self) -> None:
        pass

    async def run(self) -> None:
        await self.wait_to_run()
        return await self._action

    def start(self) -> None:
        self._action_task = asyncio.create_task(self.run())

    def __await__(self):
        if not self._action_task:
            raise RuntimeError('Trying to wait on not-started action.')
        return self._action_task.__await__()
