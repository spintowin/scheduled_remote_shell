import asyncio
import datetime
from typing import Coroutine

from scheduled_remote_shell.framework.executables.action.iaction import IAction


class ScheduledAction(IAction):
    def __init__(self, action: Coroutine, scheduled_time: datetime.datetime):
        super().__init__(action)
        self._scheduled_time = scheduled_time

    async def wait_to_run(self):
        await asyncio.sleep((self._scheduled_time - datetime.datetime.now()).total_seconds())
