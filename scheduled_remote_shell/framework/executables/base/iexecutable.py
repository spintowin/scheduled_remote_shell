from abc import abstractmethod


class IExecutable:
    @abstractmethod
    async def run(self):
        pass
