import asyncio
import signal

from contextlib import suppress
from aiorun import run

from scheduled_remote_shell.framework.executables.operation.ioperation import IOperation
from scheduled_remote_shell.framework.telemetries.telemetries_writer import TelemetriesWriter

class OperationRunner:
    def __init__(self, operations: [IOperation], telemetries_writer: TelemetriesWriter):
        self._operations = operations
        self._telemetries_writer = telemetries_writer
        self._shutdown_future = None

    def run(self) -> None:
        try:
            loop = asyncio.get_event_loop()
            loop.run_until_complete(self._start_all(loop))

        finally:
            self._cancel_all_tasks(loop)
            loop.run_until_complete(loop.shutdown_asyncgens())
            loop.close()

    @staticmethod
    def _cancel_all_tasks(loop: asyncio.AbstractEventLoop) -> None:
        tasks = [t for t in asyncio.all_tasks(loop=loop) if not t.done()]
        [task.cancel() for task in tasks]

        if tasks:
            loop.run_until_complete(asyncio.wait(tasks, return_when=asyncio.ALL_COMPLETED))    

    async def _start_all(self, loop: asyncio.AbstractEventLoop) -> None:
        """Runs all operations.

        Every operation should run forever, if operation is done, all operations quit.
        _shutdown_future - if signal or exception is raised, the wait finishes and shutdown occurs.

        If _start_all got canceled, it quits and run() function would do the general loop cleanup,
        and leanup of _start_all should be in "except asyncio.CancelledError". 

        If no operations supplied - quit immidiatly, and do not wait for _shutdown_future.
        """
        self._signal_handler_init(loop)
        self._shutdown_future = asyncio.Future()

        try:
            async with self._telemetries_writer:
                futures_list = [operation.run() for operation in self._operations]

                if futures_list:
                    futures_list += [self._shutdown_future]

                await asyncio.wait(futures_list, return_when=asyncio.FIRST_COMPLETED)

        except asyncio.CancelledError:
            pass

    def _signal_handler_init(self, loop: asyncio.AbstractEventLoop) -> None:
        [loop.add_signal_handler(sig, self._set_shutdown_future) for sig in (signal.SIGHUP, signal.SIGTERM, signal.SIGINT)]

    def _set_shutdown_future(self) -> None:
        self._shutdown_future.set_result(True)
