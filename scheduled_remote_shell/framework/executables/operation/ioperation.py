from scheduled_remote_shell.framework.executables.base.iexecutable import IExecutable


class IOperation(IExecutable):
    pass
