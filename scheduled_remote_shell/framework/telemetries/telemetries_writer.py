from scheduled_remote_shell.framework.design_patterns import singleton
from scheduled_remote_shell.framework.executables.action import immediate_action
from scheduled_remote_shell.framework.streams.writer import async_writer_base
from scheduled_remote_shell import settings


class SingletonWasNeverInitiated(BaseException):
    pass


class TelemetriesWriter(metaclass=singleton.Singleton):
    def __init__(self, async_writer: async_writer_base.AsyncWriterBase = None,
                 delimiter: str=settings.DEFAULT_TELEMETRIES_DELIMITER):
        if not hasattr(self, '_async_writer') and not async_writer:
            raise SingletonWasNeverInitiated('TelemetriesWriter never received async_writer')

        self._async_writer = async_writer
        self._delimiter = delimiter

    async def __aenter__(self):
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        await self.close()

    def write_telemetry(self, telemetry: str):
        action = immediate_action.ImmediateAction(self._async_writer.write(f'{telemetry}{self._delimiter}'))
        action.start()

    @classmethod
    def write(cls, telemetry: str):
        cls().write_telemetry(telemetry)

    async def close(self):
        await self._async_writer.close()

