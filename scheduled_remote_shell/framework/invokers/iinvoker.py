from abc import abstractmethod

from scheduled_remote_shell.framework.executables.action.iaction import IAction
from scheduled_remote_shell.protos.commands.base_command_pb2 import BaseCommand

class IInvoker:
    @abstractmethod
    def invoke(self, command: BaseCommand) -> IAction:
        pass
