import datetime

from .iinvoker import IInvoker
from scheduled_remote_shell.framework.executables.action.iaction import IAction
from scheduled_remote_shell.framework.executables.action.scheduled_action import ScheduledAction
from scheduled_remote_shell.framework.command_handler.icommand_handler import ICommandHandler
from scheduled_remote_shell.protos.commands.base_command_pb2 import BaseCommand

class CommandInvoker(IInvoker):
    def __init__(self, handlers: [ICommandHandler]):
        self._handlers = handlers

    def invoke(self, command: BaseCommand) -> IAction:
        specific_command = getattr(command, command.WhichOneof('specific_command'))
        if command.HasField('scheduled_time'):
            scheduled_time = command.scheduled_time.ToDatetime()
        else:
            scheduled_time = datetime.datetime.fromtimestamp(0)

        for h in self._handlers:
            if h.get_command_type() == type(specific_command):
                action = ScheduledAction(h.handle(specific_command), scheduled_time)
                action.start()
                return action