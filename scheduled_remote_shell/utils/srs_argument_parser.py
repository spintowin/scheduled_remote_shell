import argparse
from .srs_argument_types import LimitedInt, IPAddress


class SRSArgumentParser(argparse.ArgumentParser):
    def __init__(self):
        super(SRSArgumentParser, self).__init__()
        self.add_argument('--protobuf-commands-path', required=True)
        self.add_argument('--telemetry-output-path', required=True)
