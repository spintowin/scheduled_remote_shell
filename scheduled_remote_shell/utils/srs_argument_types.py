import argparse


class IPAddress:
    def __call__(self, addr: str):
        if len(addr.split('.')) != 4:
            raise argparse.ArgumentError()

        for byte in addr.split('.'):
            if not byte.isdigit() or int(byte) > 255:
                raise argparse.ArgumentError()

        return addr


class LimitedInt:
    def __init__(self, min: int, max: int):
        self.min = min
        self.max = max

    def __call__(self, n: int):
        try:
            result = int(n)
        except ValueError:
            raise argparse.ArgumentError()
        
        if result < self.min or result > self.max:
            raise argparse.ArgumentError()

        return result
