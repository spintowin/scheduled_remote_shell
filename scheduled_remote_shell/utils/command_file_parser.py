class CommandParser:
    @staticmethod
    def parse_bash_command_file(path: str) -> [str]:
        commands = []
        with open(path, 'r') as f:
            for line in f.readlines():
                commands.append(line.strip())

        return commands
