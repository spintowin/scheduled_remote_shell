import asyncio

from scheduled_remote_shell.remote_shell import ScheduledRemoteShell
from scheduled_remote_shell.utils import srs_argument_parser


def main():
    args = srs_argument_parser.SRSArgumentParser().parse_args()
    srs = ScheduledRemoteShell(args.protobuf_commands_path, args.telemetry_output_path)
    srs.run()

if __name__ == '__main__':
    main()
