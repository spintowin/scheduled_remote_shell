
from scheduled_remote_shell.operations.remote_shell_operation import RemoteShellOperation
from scheduled_remote_shell.framework.invokers.command_invoker import CommandInvoker
from scheduled_remote_shell.framework.command_handler.execute_bash_command_handler import ExecuteBashCommandHandler
from scheduled_remote_shell.utils.command_file_parser import CommandParser
from scheduled_remote_shell.framework.telemetries.telemetries_writer import TelemetriesWriter
from scheduled_remote_shell.framework.streams.writer.async_text_writer import AsyncTextWriter
from scheduled_remote_shell.framework.executables.operation.ioperation import IOperation
from scheduled_remote_shell.framework.executables.utils.operation_runner import OperationRunner


class ScheduledRemoteShell:
    def __init__(self, protobuf_commands_path: str, telemetries_file_path: str):
        self._protobuf_commands_path = protobuf_commands_path
        self._telemetries_writer = TelemetriesWriter(AsyncTextWriter(open(telemetries_file_path, 'w')))

    def _get_operations(self) -> [IOperation]:
        return []

    def run(self) -> None:
        operation_runner = OperationRunner(self._get_operations(), self._telemetries_writer)
        operation_runner.run()
