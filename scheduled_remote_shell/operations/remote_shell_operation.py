from argparse import Namespace

from scheduled_remote_shell.framework.executables.operation.ioperation import IOperation
from scheduled_remote_shell.framework.invokers.iinvoker import IInvoker


class RemoteShellOperation(IOperation):
    def __init__(self, invoker: IInvoker, command: Namespace):
        super().__init__()
        self._invoker = invoker
        self._command = command

    async def run(self) -> None:
        await self._invoker.invoke(self._command)
